package com.jobcircular.android.ApiService;

import com.jobcircular.android.Models.AppInfo;
import com.jobcircular.android.Models.category.Category;
import com.jobcircular.android.Models.circular.CircularList;
import com.jobcircular.android.Models.circular_details.CircularDetails;
import com.jobcircular.android.Models.featured.HomeCircular;
import com.jobcircular.android.Models.sub_category.SubCategoryList;
import com.jobcircular.android.Utils.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {

    @GET(Constants.APP_INFO)
    Call<AppInfo> getApplicationInfo();

    @GET(Constants.CATE_LIST)
    Call<Category> getCategoryList();


    @GET(Constants.BASE_API)
    public Call<CircularList> getCircularListBySubCategory(@Query("subcategory_id") String id);

    @GET(Constants.BASE_API)
    public Call<SubCategoryList> getSubCategoryListByCategoryId(@Query("subcategory_by_cat_id") String id);

    @GET(Constants.BASE_API)
    public Call<CircularList> getCircularByKeyward(@Query("search_text") String keyward);

    @GET(Constants.BASE_API)
    public Call<CircularDetails> getSingleCircularDetailsById(@Query("circular_id") String id);

    @GET(Constants.HOME_API)
    public Call<HomeCircular> getHomeCircular();


}

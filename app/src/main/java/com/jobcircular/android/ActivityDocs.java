package com.jobcircular.android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.common.util.IOUtils;

import java.io.IOException;
import java.io.InputStream;

public class ActivityDocs extends AppCompatActivity {

    String title;
    String message;
    String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docs);

        Intent intent = getIntent();
        if (intent != null) {
            title = intent.getStringExtra("title");
            message = intent.getStringExtra("message");
            key = intent.getStringExtra("key");
        }


        TextView titileOFDocs = findViewById(R.id.titileOFDocs);
        TextView descripitionOfDocs = findViewById(R.id.descripitionOfDocs);

        if (title != null && !TextUtils.isEmpty(title)) {
            titileOFDocs.setText(title);

        }

        //termscon.html



        if (message != null && !TextUtils.isEmpty(message)) {
            if (message.equals("terms")){
                String terms_and_conditions = getResources().getString(R.string.terms_and_conditions);
                Log.d("sfdgdfsgdfsgs", "onCreate: "+terms_and_conditions);
                descripitionOfDocs.setText(terms_and_conditions);
            }else {
                descripitionOfDocs.setText(Html.fromHtml(message));
            }


        }
    }
}

package com.jobcircular.android.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jobcircular.android.Activity.ActivityAgeCalculator;
import com.jobcircular.android.Activity.ActivityCircularView;
import com.jobcircular.android.Activity.MainActivity;
import com.jobcircular.android.Models.Category;
import com.jobcircular.android.R;

import java.util.List;

public class NavCategoryAdapter extends RecyclerView.Adapter<NavCategoryAdapter.CategoryHolder> {

    Context ct;
    List<Category> list;

    public NavCategoryAdapter(Context ct, List<Category> list) {
        this.ct = ct;
        this.list = list;
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(ct).inflate(R.layout.item_sub_category,parent,false);
        return new CategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {

        Category category=list.get(position);

        if (category !=null){

            if (category.isHeadline()){
                holder.headline.setVisibility(View.VISIBLE);
                holder.headline.setClickable(false);
                holder.headTitle.setClickable(false);
                holder.content.setVisibility(View.GONE);
                holder.headTitle.setText(category.getHeadLine());
            }else {
                holder.headline.setVisibility(View.GONE);
                holder.content.setVisibility(View.VISIBLE);
                holder.title.setText(category.getTitle());
                holder.content.setClickable(true);

                if (category.getIcon() != 0){
                    holder.image.setImageResource(category.getIcon());
                }

                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (category.getId()=="100"){
                            ((MainActivity)ct).closeDrawer();
                            ct.startActivity(new Intent(ct, ActivityAgeCalculator.class));
                        }else {
                            ((MainActivity) ct).closeDrawer();
                            ct.startActivity(new Intent(ct, ActivityCircularView.class)
                                    .putExtra("cat_id", category.getId())
                                    .putExtra("cat_name", category.getTitle()));
                        }
                    }
                });

            }


        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CategoryHolder extends RecyclerView.ViewHolder{

        LinearLayout headline;
        LinearLayout content;
        TextView title;
        TextView headTitle;
        View view;
        ImageView image;

        public CategoryHolder(@NonNull View itemView) {
            super(itemView);
            headTitle=itemView.findViewById(R.id.headTitle);
            title=itemView.findViewById(R.id.title);
            content=itemView.findViewById(R.id.content);
            headline=itemView.findViewById(R.id.headline);
            image=itemView.findViewById(R.id.image);
            view=itemView;
        }
    }
}

package com.jobcircular.android.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jobcircular.android.Activity.ActivityCircularDetails;
import com.jobcircular.android.Data.App;
import com.jobcircular.android.Models.circular.Circular;
import com.jobcircular.android.R;

import java.util.List;

public class CircularAdapter extends RecyclerView.Adapter<CircularAdapter.CircularHolder> {


    Context context;
    List<Circular> list;

    public CircularAdapter(Context context, List<Circular> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public CircularHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CircularHolder(LayoutInflater.from(context).inflate(R.layout.item_circular,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull CircularHolder holder, int position) {

        Circular circular=list.get(position);

        if (circular !=null){
            holder.title.setText((circular.getCircularTitle()).trim());
            holder.category_title.setText((circular.getSubcategoryName()).trim());

            if (circular.getNameOfPosition() !=null &&  !TextUtils.isEmpty(circular.getNameOfPosition())){
                holder.positions.setText((circular.getNameOfPosition()).trim());
            }else {
                holder.positions.setVisibility(View.GONE);
            }

            if (circular.getDeadline() !=null && !TextUtils.isEmpty(circular.getDeadline())){
                String replacedOne = circular.getDeadline().replaceAll("0","০").replaceAll("1","১").replaceAll("2","২").replaceAll("3","৩").replaceAll("4","৪").replaceAll("5","৫").replaceAll("6","৬").replaceAll("7","৭").replaceAll("8","৮").replaceAll("9","৯");
                holder.featured.setText((context.getString(R.string.deadline)+" "+App.reverseDate(replacedOne)).trim());
            }else {
                holder.featured.setVisibility(View.GONE);
            }


            if (circular.getPostdate() !=null && !TextUtils.isEmpty(circular.getPostdate())){
                holder.postdate.setText(App.reverseDate(App.replaceBanglaFont(circular.getPostdate().trim())));
            }else {
                holder.postdate.setVisibility(View.GONE);
            }





            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, ActivityCircularDetails.class).putExtra("cir_id",circular.getId()));
                }
            });


        }


    }






    @Override
    public int getItemCount() {
        return list.size();
    }

    class CircularHolder extends RecyclerView.ViewHolder{

        TextView title;
        TextView category_title;
        TextView featured;
        TextView positions;
        TextView postdate;

        View view;

        public CircularHolder(@NonNull View itemView) {
            super(itemView);
            view=itemView;
            category_title=itemView.findViewById(R.id.category_title);
            title=itemView.findViewById(R.id.title);
            featured=itemView.findViewById(R.id.featured);
            positions=itemView.findViewById(R.id.positions);
            postdate=itemView.findViewById(R.id.postdate);
        }
    }
}

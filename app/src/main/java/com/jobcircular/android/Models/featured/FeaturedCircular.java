
package com.jobcircular.android.Models.featured;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeaturedCircular {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("aid")
    @Expose
    private String aid;
    @SerializedName("circular_title")
    @Expose
    private String circularTitle;
    @SerializedName("circular_cover_img")
    @Expose
    private String circularCoverImg;
    @SerializedName("circular_bg_img")
    @Expose
    private String circularBgImg;
    @SerializedName("circular_file_type")
    @Expose
    private String circularFileType;
    @SerializedName("total_rate")
    @Expose
    private String totalRate;
    @SerializedName("rate_avg")
    @Expose
    private String rateAvg;
    @SerializedName("circular_views")
    @Expose
    private String circularViews;
    @SerializedName("subcategory_id")
    @Expose
    private String subcategoryId;
    @SerializedName("subcategory_name")
    @Expose
    private String subcategoryName;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("deadline")
    @Expose
    private Object deadline;
    @SerializedName("postdate")
    @Expose
    private Object postdate;
    @SerializedName("name_of_position")
    @Expose
    private Object nameOfPosition;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getCircularTitle() {
        return circularTitle;
    }

    public void setCircularTitle(String circularTitle) {
        this.circularTitle = circularTitle;
    }

    public String getCircularCoverImg() {
        return circularCoverImg;
    }

    public void setCircularCoverImg(String circularCoverImg) {
        this.circularCoverImg = circularCoverImg;
    }

    public String getCircularBgImg() {
        return circularBgImg;
    }

    public void setCircularBgImg(String circularBgImg) {
        this.circularBgImg = circularBgImg;
    }

    public String getCircularFileType() {
        return circularFileType;
    }

    public void setCircularFileType(String circularFileType) {
        this.circularFileType = circularFileType;
    }

    public String getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(String totalRate) {
        this.totalRate = totalRate;
    }

    public String getRateAvg() {
        return rateAvg;
    }

    public void setRateAvg(String rateAvg) {
        this.rateAvg = rateAvg;
    }

    public String getCircularViews() {
        return circularViews;
    }

    public void setCircularViews(String circularViews) {
        this.circularViews = circularViews;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getSubcategoryName() {
        return subcategoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
        this.subcategoryName = subcategoryName;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Object getDeadline() {
        return deadline;
    }

    public void setDeadline(Object deadline) {
        this.deadline = deadline;
    }

    public Object getPostdate() {
        return postdate;
    }

    public void setPostdate(Object postdate) {
        this.postdate = postdate;
    }

    public Object getNameOfPosition() {
        return nameOfPosition;
    }

    public void setNameOfPosition(Object nameOfPosition) {
        this.nameOfPosition = nameOfPosition;
    }

}

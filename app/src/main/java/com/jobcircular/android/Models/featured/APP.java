
package com.jobcircular.android.Models.featured;

import java.util.List;

import com.jobcircular.android.Models.circular.Circular;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class APP {

    @SerializedName("featured_circulars")
    @Expose
    private List<Circular> featuredCirculars = null;
    @SerializedName("latest_circulars")
    @Expose
    private List<Circular> latestCirculars = null;
    @SerializedName("popular_circulars")
    @Expose
    private List<Circular> popularCirculars = null;

    public List<Circular> getFeaturedCirculars() {
        return featuredCirculars;
    }

    public void setFeaturedCirculars(List<Circular> featuredCirculars) {
        this.featuredCirculars = featuredCirculars;
    }

    public List<Circular> getLatestCirculars() {
        return latestCirculars;
    }

    public void setLatestCirculars(List<Circular> latestCirculars) {
        this.latestCirculars = latestCirculars;
    }

    public List<Circular> getPopularCirculars() {
        return popularCirculars;
    }

    public void setPopularCirculars(List<Circular> popularCirculars) {
        this.popularCirculars = popularCirculars;
    }

}

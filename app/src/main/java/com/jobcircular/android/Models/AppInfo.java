
package com.jobcircular.android.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppInfo {

    @SerializedName("APP")
    @Expose
    private List<APP> APP = null;

    public List<APP> getEBOOKAPP() {
        return APP;
    }

    public void setEBOOKAPP(List<APP> APP) {
        this.APP = APP;
    }



}
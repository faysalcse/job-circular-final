package com.jobcircular.android.Models.sub_category;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubCategoryList {

    @SerializedName("APP")
    @Expose
    private List<SubCategory> getSubCategory = null;

    public List<SubCategory> getgetSubCategory() {
        return getSubCategory;
    }

    public void setgetSubCategory(List<SubCategory> getSubCategory) {
        this.getSubCategory = getSubCategory;
    }

}
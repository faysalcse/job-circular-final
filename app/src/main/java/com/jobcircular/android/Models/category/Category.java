
package com.jobcircular.android.Models.category;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("APP")
    @Expose
    private List<JobsCategory> eJOBSAPP = null;

    public List<JobsCategory> getEJOBSAPP() {
        return eJOBSAPP;
    }

    public void setEJOBSAPP(List<JobsCategory> eJOBSAPP) {
        this.eJOBSAPP = eJOBSAPP;
    }

}


package com.jobcircular.android.Models.circular;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CircularList {

    @SerializedName("APP")
    @Expose
    private List<Circular> CircularList = null;

    public List<Circular> getCircularList() {
        return CircularList;
    }

    public void setCircularList(List<Circular> CircularList) {
        this.CircularList = CircularList;
    }

}

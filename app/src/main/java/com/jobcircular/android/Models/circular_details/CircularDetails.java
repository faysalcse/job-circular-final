
package com.jobcircular.android.Models.circular_details;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CircularDetails {

    @SerializedName("APP")
    @Expose
    private List<EJOBSAPP> eJOBSAPP = null;

    public List<EJOBSAPP> getEJOBSAPP() {
        return eJOBSAPP;
    }

    public void setEJOBSAPP(List<EJOBSAPP> eJOBSAPP) {
        this.eJOBSAPP = eJOBSAPP;
    }

}

package com.jobcircular.android.Models;

public class Category {
    String id;
    String title;
    String headLine;
    int icon;
    boolean isHeadline;

    public Category(String id,String title, int icon, boolean isHeadline) {
        this.id = id;
        this.title = title;
        this.icon = icon;
        this.isHeadline = isHeadline;
    }

    public Category(String headLine, boolean isHeadline) {
        this.headLine = headLine;
        this.isHeadline = isHeadline;
    }


    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHeadLine() {
        return headLine;
    }

    public void setHeadLine(String headLine) {
        this.headLine = headLine;
    }

    public boolean isHeadline() {
        return isHeadline;
    }

    public void setHeadline(boolean headline) {
        isHeadline = headline;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}


package com.jobcircular.android.Models.featured;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeCircular {

    @SerializedName("APP")
    @Expose
    private APP aPP;

    public APP getAPP() {
        return aPP;
    }

    public void setAPP(APP aPP) {
        this.aPP = aPP;
    }

}

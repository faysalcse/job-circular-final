package com.jobcircular.android.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = Circular.class,version = 1)
public abstract class CircularDatabase extends RoomDatabase {
    private static CircularDatabase instance;

    public abstract CircularDao circularDao();

    public static synchronized CircularDatabase getInstance(Context context){
        if (instance==null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    CircularDatabase.class,"circular_database")
                    .fallbackToDestructiveMigration()
                    .build();

        }
        return instance;
    }

}

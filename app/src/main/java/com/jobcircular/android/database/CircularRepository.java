package com.jobcircular.android.database;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class CircularRepository implements AsyncResponse  {
    private CircularDao circularDao;
    private LiveData<List<Circular>> allcircular;

    public CircularRepository(Application application){
        CircularDatabase database = CircularDatabase.getInstance(application);
        circularDao = database.circularDao();
        allcircular = circularDao.getAllCircular();
    }

    public void insert(Circular note){
        new InsertNoteAsyncTask(circularDao).execute(note);
    }

    public void update(Circular note){
        new UpdateNoteAsyncTask(circularDao).execute(note);
    }

    public void delete(int id){
        new DeleteNoteAsyncTask(circularDao).execute(id);

    }

    public void checkExists(int cid,AsyncResponse response){
        new CheckExitsAsyncTask(circularDao,response).execute(cid);
    }

    public void deleteAllcircular(){
        new DeleteAllNoteAsyncTask(circularDao).execute();
    }

    public LiveData<List<Circular>> getAllcircular(){
        return allcircular;
    }

    @Override
    public void processFinish(boolean output) {

    }


    private static class InsertNoteAsyncTask extends AsyncTask<Circular,Void,Void>{

        private CircularDao circularDao;
        private InsertNoteAsyncTask(CircularDao circularDao){
            this.circularDao = circularDao;
        }

        @Override
        protected Void doInBackground(Circular... circular) {
            circularDao.insert(circular[0]);
            return null;
        }
    }


    private static class CheckExitsAsyncTask extends AsyncTask<Integer, Void, Boolean> {
        private CircularDao circularDao;
        AsyncResponse response;
        private CheckExitsAsyncTask(CircularDao circularDao, AsyncResponse response){
            this.circularDao = circularDao;
            this.response = response;
        }

        @Override
        protected Boolean doInBackground(Integer... integers) {
            try {
                Circular circular = circularDao.loadCircularById(integers[0]);
                if (circular !=null){
                    return true;
                }
            }catch (Exception e){
                return false;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            response.processFinish(aBoolean);
        }
    }


    private static class UpdateNoteAsyncTask extends AsyncTask<Circular,Void,Void>{

        private CircularDao circularDao;
        private UpdateNoteAsyncTask(CircularDao circularDao){
            this.circularDao = circularDao;
        }

        @Override
        protected Void doInBackground(Circular... circular) {
            circularDao.update(circular[0]);
            return null;
        }
    }
    private static class DeleteNoteAsyncTask extends AsyncTask<Integer,Void,Void>{

        private CircularDao circularDao;

        public DeleteNoteAsyncTask(CircularDao circularDao) {
            this.circularDao = circularDao;
        }


        @Override
        protected Void doInBackground(Integer... integers) {
            circularDao.delete(integers[0]);
            return null;
        }
    }
    private static class DeleteAllNoteAsyncTask extends AsyncTask<Void,Void,Void>{
        private CircularDao circularDao;

        private DeleteAllNoteAsyncTask(CircularDao circularDao){
            this.circularDao = circularDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            circularDao.deleteAllCircular();
            return null;
        }
    }


}

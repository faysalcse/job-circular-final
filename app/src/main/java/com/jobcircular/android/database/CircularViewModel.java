package com.jobcircular.android.database;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class CircularViewModel extends AndroidViewModel {

    private CircularRepository repository;
    private LiveData<List<Circular>> allNotes;

    public CircularViewModel(@NonNull Application application) {
        super(application);
        repository=new CircularRepository(application);
        allNotes = repository.getAllcircular();
    }

    public void insert(Circular circular){
        repository.insert(circular);
    }
    public void update(Circular circular){
        repository.update(circular);
    }
    public void delete(int id){
        repository.delete(id);
    }
    public void deleteAllNotes(){
        repository.deleteAllcircular();
    }

    public LiveData<List<Circular>> getAllNotes(){
        return allNotes;
    }

    public void getExists(int cid,AsyncResponse response){
        repository.checkExists(cid,response);
    }




}

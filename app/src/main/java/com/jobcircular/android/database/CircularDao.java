package com.jobcircular.android.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CircularDao {
    @Insert
    void insert(Circular circularDao);

    @Update
    void update(Circular circularDao);

    @Query("DELETE FROM circular  WHERE circularId = :id")
    void delete(int id);

    @Query("DELETE FROM circular")
    void deleteAllCircular();

    @Query("SELECT * FROM circular")
    LiveData<List<Circular>> getAllCircular();

    @Query("SELECT * FROM Circular WHERE circularId = :id")
    Circular loadCircularById(int id);
}

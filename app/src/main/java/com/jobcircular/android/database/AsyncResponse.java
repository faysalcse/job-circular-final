package com.jobcircular.android.database;

public interface AsyncResponse {
    void processFinish(boolean output);
}

package com.jobcircular.android.database;

import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "circular")
public class Circular {

    @PrimaryKey(autoGenerate = true)
    int id;
    String circularId;
    String title;
    String nameOfPosition;
    String subCategoryName;
    String deadline;
    String postDate;


    public Circular(String circularId, String title, String nameOfPosition, String subCategoryName, String deadline, String postDate) {
        this.circularId = circularId;
        this.title = title;
        this.nameOfPosition = nameOfPosition;
        this.subCategoryName = subCategoryName;
        this.deadline = deadline;
        this.postDate = postDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCircularId() {
        return circularId;
    }

    public void setCircularId(String circularId) {
        this.circularId = circularId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNameOfPosition() {
        return nameOfPosition;
    }

    public void setNameOfPosition(String nameOfPosition) {
        this.nameOfPosition = nameOfPosition;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    @Override
    public String toString() {
        return "Circular{" +
                "id=" + id +
                ", circularId='" + circularId + '\'' +
                ", title='" + title + '\'' +
                ", nameOfPosition='" + nameOfPosition + '\'' +
                ", subCategoryName='" + subCategoryName + '\'' +
                ", deadline='" + deadline + '\'' +
                ", postDate='" + postDate + '\'' +
                '}';
    }
}

package com.jobcircular.android.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.navigation.NavigationView;
import com.jobcircular.android.ActivityDocs;
import com.jobcircular.android.Adapter.NavCategoryAdapter;
import com.jobcircular.android.Fragments.HomeFragment;
import com.jobcircular.android.Models.Category;
import com.jobcircular.android.R;
import com.jobcircular.android.Utils.Constants;
import com.jobcircular.android.Utils.SharedPref;
import com.jobcircular.android.Utils.Tools;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "RetrofitOperations";
    Context context;
    AdView mAdView;
    private ActionBar actionBar;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this);
        context = this;

        initToolbar();
        initDrawerMenu();
        intiBannerAds();


        setUpInterstitialAd();


    }

    private void setUpInterstitialAd() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.interstital_ads_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {


                                          @Override
                                          public void onAdLoaded() {
                                              super.onAdLoaded();
                                              Handler handler = new Handler();
                                              handler.postDelayed(new Runnable() {
                                                  public void run() {
                                                      displayInterstitial();
                                                  }
                                              }, 60000);

                                          }
                                      }
        );

    }

    private void displayInterstitial() {

        if (mInterstitialAd != null) {

            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        }


    }


    private void intiBannerAds() {
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorTextAction), PorterDuff.Mode.SRC_ATOP);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.colorTextAction));
        Tools.setSmartSystemBar(this);

    }

    private void initDrawerMenu() {
        NavigationView nav_view = findViewById(R.id.nav_view);
        drawer = findViewById(R.id.drawer_layout);
        TextView name = nav_view.findViewById(R.id.name);
        TextView settings = nav_view.findViewById(R.id.settings);
        ImageView avatar = nav_view.findViewById(R.id.avatar);
        RecyclerView subCatList = nav_view.findViewById(R.id.subCatList);


        if (new HomeFragment() != null) {

            actionBar.setTitle("হোম");
            drawer.closeDrawers();
            openFragment(new HomeFragment());
        }


        List<Category> list = new ArrayList<>();
        list.add(new Category("পরীক্ষার খবরঃ", true));
        list.add(new Category("2", "নোটিশ বোর্ড", R.drawable.ic_notice_board, false));
        list.add(new Category("3", "পরীক্ষার সময়সূচী", R.drawable.ic_exam_time, false));
        list.add(new Category("4", "ফলাফল", R.drawable.ic_result, false));

        list.add(new Category("জব ক্যাটাগরিঃ", true));
        list.add(new Category("5", "সরকারী", R.drawable.ic_sub_category, false));
        list.add(new Category("6", "বেসরকারী", R.drawable.ic_sub_category, false));
        list.add(new Category("7", "ডিফেন্স", R.drawable.ic_sub_category, false));
        list.add(new Category("8", "মন্ত্রণালয়", R.drawable.ic_sub_category, false));
        list.add(new Category("9", "সরকারী ব্যাংক", R.drawable.ic_sub_category, false));
        list.add(new Category("10", "বেসরকারী ব্যাংক", R.drawable.ic_sub_category, false));
        list.add(new Category("11", "শিক্ষক নিয়োগ", R.drawable.ic_sub_category, false));
        list.add(new Category("12", "সিটি কর্পোরেশন", R.drawable.ic_sub_category, false));
        list.add(new Category("13", "স্কুল ও কলেজ", R.drawable.ic_sub_category, false));
        list.add(new Category("14", "বিশ্ববিদ্যালয়", R.drawable.ic_sub_category, false));
        list.add(new Category("15", "রেল ও সড়ক", R.drawable.ic_sub_category, false));
        list.add(new Category("16", "নদী ও বন্দর", R.drawable.ic_sub_category, false));
        list.add(new Category("17", "সেলস/মার্কেটিং", R.drawable.ic_sub_category, false));
        list.add(new Category("18", "মেডিকেল ও হসপিটাল", R.drawable.ic_sub_category, false));
        list.add(new Category("19", "অন্যান্য", R.drawable.ic_sub_category, false));


        list.add(new Category("বিসিএস ও নিয়োগ পরীক্ষাঃ", true));
        list.add(new Category("20", " বিগত বিসিএস প্রশ্ন", R.drawable.ic_question_bces, false));
        list.add(new Category("21", " বিগত নিয়োগ প্রশ্ন", R.drawable.ic_question_bces, false));
        list.add(new Category("22", "এনটিআরসিএ প্রশ্ন", R.drawable.ic_question_bces, false));

        list.add(new Category("জব প্রিপারেশনঃ", true));
        list.add(new Category("23", "প্রস্তুতি", R.drawable.ic_preprations, false));
        list.add(new Category("24", "উৎসাহ", R.drawable.ic_ustno, false));
        list.add(new Category("25", "মডেল টেস্ট", R.drawable.ic_model_test, false));
        list.add(new Category("26", "ইন্টারভিউ বোর্ড", R.drawable.ic_interview_board, false));

        list.add(new Category("অন্যান্য প্রয়োজনীয়ঃ", true));
        list.add(new Category("27", "ফি ও আবেদন পত্র", R.drawable.ic_interview_board, false));
        list.add(new Category("29", "জীবন বৃত্তান্ত (সিভি)", R.drawable.ic_interview_board, false));
        list.add(new Category("100", "বয়স ক্যালকুলেটর", R.drawable.ic_calclutor, false));


        NavCategoryAdapter adapter = new NavCategoryAdapter(context, list);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        subCatList.setLayoutManager(llm);
        subCatList.setAdapter(adapter);


        drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //  showInterstitial();
            }
        });
    }

    public void closeDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawers();

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void onDrawerMenuClick(View view) {
        Fragment fragment = null;
        Fragment fragementHome = new HomeFragment();
        String title = actionBar.getTitle().toString();
        int menu_id = view.getId();
        switch (menu_id) {

            case R.id.nav_age_calculator:
                startActivity(new Intent(this, ActivityAgeCalculator.class));
                break;

            case R.id.nav_menu_about:
                startActivity(new Intent(this, ActivityAboutUS.class));
                break;

            case R.id.nav_menu_category:
//                startActivity(new Intent(this, ActivityJobsCategory.class));
                break;


            case R.id.nav_menu_bookmark:
                startActivity(new Intent(this, BookMarkActivity.class));
                break;

            case R.id.nav_menu_home:
                if (new HomeFragment() == null) {
                   /* finish();
                    startActivity(new Intent(this, MainActivity.class));*/
                    fragment = new HomeFragment();
                }
                fragment = fragementHome;
                title = getString(R.string.title_menu_home);
                break;


            case R.id.nav_privacy_policy:
                startActivity(new Intent(MainActivity.this, ActivityDocs.class).putExtra("message", SharedPref.getKey(context, Constants.APP_PRIVACY_POLICY)).putExtra("title", getString(R.string.title_privacy_policy)));
                break;
            case R.id.nav_menu_trams_conditions:

                Log.d(TAG, "onDrawerMenuClick: ");

                startActivity(new Intent(MainActivity.this, ActivityDocs.class).putExtra("message", "terms").putExtra("title", getString(R.string.title_trams_and_conditions)));
                break;
            case R.id.nav_menu_rate:
                Tools.rateAction(this);
                break;
        }
        actionBar.setTitle(title);
        drawer.closeDrawers();
        if (fragment != null) openFragment(fragment);
    }


    private void openFragment(Fragment fragment) {
        clearStack();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_container, new HomeFragment());
        transaction.commit();


    }

    public void clearStack() {
        //Here we are clearing back stack fragment entries
        int backStackEntry = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntry > 0) {
            for (int i = 0; i < backStackEntry; i++) {
                getSupportFragmentManager().popBackStackImmediate();
            }
        }

        //Here we are removing all the fragment that are shown here
        if (getSupportFragmentManager().getFragments() != null && getSupportFragmentManager().getFragments().size() > 0) {
            for (int i = 0; i < getSupportFragmentManager().getFragments().size(); i++) {
                Fragment mFragment = getSupportFragmentManager().getFragments().get(i);
                if (mFragment != null) {
                    getSupportFragmentManager().beginTransaction().remove(mFragment).commit();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.colorTextAction));

      /*  final MenuItem menuItem = menu.findItem(R.id.action_search);
        View actionView = MenuItemCompat.getActionView(menuItem);

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menu_id = item.getItemId();
        if (menu_id == android.R.id.home) {
            if (!drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.openDrawer(GravityCompat.START);
            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        } else if (menu_id == R.id.action_search) {
            startActivity(new Intent(context, SearchActivity.class));
        } else if (menu_id == R.id.action_refresh) {
            Toast.makeText(context, "This app is underdevelopment", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }


    private void gettingApplicationInfo() {

    /*    Log.d(TAG, "gettingApplicationInfo: ");

        ProgressDialog dialog=new ProgressDialog(MainActivity.this);
        dialog.setMessage("Please wait");
        dialog.show();

        Call<AppInfo> getInfoCall=apiInterface.getApplicationInfo();
        getInfoCall.enqueue(new Callback<AppInfo>() {

            @Override
            public void onResponse(Call<AppInfo> call, Response<AppInfo> response) {
               Log.d(TAG, "onResponse: "+response.body().toString());

                if (response.isSuccessful() && response.body() !=null){
                    AppInfo appinfo=response.body();
                    SharedPref.putKey(context, Constants.APP_INFO,"yes");
                    SharedPref.putKey(context, Constants.APP_NAME,appinfo.getEBOOKAPP().get(0).getAppName());
                    SharedPref.putKey(context, Constants.APP_LOGO,appinfo.getEBOOKAPP().get(0).getAppLogo());
                    SharedPref.putKey(context, Constants.APP_VERSION,appinfo.getEBOOKAPP().get(0).getAppVersion());
                    SharedPref.putKey(context, Constants.APP_AUTHOR,appinfo.getEBOOKAPP().get(0).getAppAuthor());
                    SharedPref.putKey(context, Constants.APP_CONTACT,appinfo.getEBOOKAPP().get(0).getAppContact());
                    SharedPref.putKey(context, Constants.APP_EMAIL,appinfo.getEBOOKAPP().get(0).getAppEmail());
                    SharedPref.putKey(context, Constants.APP_WEBSITE,appinfo.getEBOOKAPP().get(0).getAppWebsite());
                    SharedPref.putKey(context, Constants.APP_DESCRIPTION,appinfo.getEBOOKAPP().get(0).getAppDescription());
                    SharedPref.putKey(context, Constants.APP_PRIVACY_POLICY,appinfo.getEBOOKAPP().get(0).getAppPrivacyPolicy());
                }else {
                    SharedPref.putKey(context, Constants.APP_INFO,"no");
                }



                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AppInfo> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Failed to retrive data", Toast.LENGTH_SHORT).show();
                SharedPref.putKey(context, Constants.APP_INFO,"no");
                dialog.dismiss();
            }
        });
*/
    }
}

package com.jobcircular.android.Activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jobcircular.android.Adapter.CircularAdapter;
import com.jobcircular.android.ApiService.APIClient;
import com.jobcircular.android.ApiService.APIInterface;
import com.jobcircular.android.Data.App;
import com.jobcircular.android.Models.circular.Circular;
import com.jobcircular.android.Models.circular.CircularList;
import com.jobcircular.android.R;
import com.jobcircular.android.Utils.Tools;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCircularView extends AppCompatActivity {


    Toolbar toolbar;
    APIInterface apiInterface;

    @BindView(R.id.circularList)
    RecyclerView circularList;


    @BindView(R.id.contentLayout)
    LinearLayout contentLayout;

    @BindView(R.id.failed_layout)
    LinearLayout failed_layout;

    @BindView(R.id.failed_message)
    TextView failed_message;

    @BindView(R.id.failed_retry)
    Button failed_retry;

    @BindView(R.id.progressBar)
    LinearLayout progressBar;

     String id=null;

    Context context;

    public static final String TAG=ActivityCircularView.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circular_view);
        context=this;
        ButterKnife.bind(this);
        apiInterface= APIClient.getClient().create(APIInterface.class);

        Intent intent=getIntent();
        if (intent !=null){
            String title=intent.getStringExtra("cat_name");
            id=intent.getStringExtra("cat_id");

            if (title !=null && !TextUtils.isEmpty(title))
                    initToolbar(title);

            if (id !=null && !TextUtils.isEmpty(id))
                    initJobCircularBySubCategory(id);



        }


    }

    private void initToolbar(String title) {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.colorTextAction));
        Tools.setSmartSystemBar(this);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }


    private void initJobCircularBySubCategory(String catId) {
        Log.d(TAG, "initJobCircularBySubCategory: "+catId);
        inintErrorLayout(View.GONE,"");
        progressBar.setVisibility(View.VISIBLE);
        circularList.setVisibility(View.GONE);

        Call<CircularList> call=apiInterface.getCircularListBySubCategory(catId);
        call.enqueue(new Callback<CircularList>() {
            @Override
            public void onResponse(Call<CircularList> call, Response<CircularList> response) {

                if (response.isSuccessful()){
                    List<Circular> circular_List=response.body().getCircularList();
                    if (circular_List.size() !=0){
                        initJobCircularView(circular_List);
                        progressBar.setVisibility(View.GONE);
                        circularList.setVisibility(View.VISIBLE);
                        inintErrorLayout(View.GONE,"");
                    }else {
                        inintErrorLayout(View.VISIBLE,getString(R.string.error_nodata));
                        progressBar.setVisibility(View.GONE);
                        circularList.setVisibility(View.GONE);
                    }
                }

            }

            @Override
            public void onFailure(Call<CircularList> call, Throwable t) {
                circularList.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                // Toasty.warning(ActivityJobsCategory.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                inintErrorLayout(View.VISIBLE,getString(R.string.error_server));
            }
        });

    }


    private void inintErrorLayout(int visivility,String  msg){
        switch (visivility){
            case View.GONE:
                failed_layout.setVisibility(View.GONE);
                contentLayout.setVisibility(View.VISIBLE);
                break;
            case View.VISIBLE:
                failed_layout.setVisibility(View.VISIBLE);
                contentLayout.setVisibility(View.GONE);
                break;
        }
        if (msg !=null){
            failed_message.setText(msg);
            failed_retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (App.isInternetOn(context)){
                        initJobCircularBySubCategory(id);
                    }else {
                        failed_layout.setVisibility(View.VISIBLE);
                        failed_message.setText(getString(R.string.error_internet));
                    }
                }
            });
        }
    }

    private void initJobCircularView(List<Circular> data) {
        CircularAdapter adapter=new CircularAdapter(context,data);
        LinearLayoutManager linearLayoutManager
                = new LinearLayoutManager(ActivityCircularView.this, LinearLayoutManager.VERTICAL, false);
      /*  DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(circularList.getContext(),
                linearLayoutManager.getOrientation());
        circularList.addItemDecoration(dividerItemDecoration);*/
        circularList.setLayoutManager(linearLayoutManager);
        circularList.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }else if (item.getItemId()==R.id.action_refresh){
           initJobCircularBySubCategory(id);
        }
        return super.onOptionsItemSelected(item);
    }

}

package com.jobcircular.android.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import com.jobcircular.android.Data.App;
import com.jobcircular.android.R;
import com.jobcircular.android.Utils.Constants;
import com.jobcircular.android.Utils.SharedPref;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityAboutUS extends AppCompatActivity {

    Toolbar mToolbar;

    @BindView(R.id.textView_app_name_about_us)
    TextView appName;

     @BindView(R.id.textView_app_version_about_us)
    TextView appVersion;

     @BindView(R.id.textView_app_author_about_us)
    TextView appAuthor;

     @BindView(R.id.textView_app_email_about_us)
    TextView appAuthorEmail;

     @BindView(R.id.textView_app_website_about_us)
    TextView appAuthorWebsite;

     @BindView(R.id.textView_app_contact_about_us)
    TextView getAppAuthorContact;

     @BindView(R.id.textView_app_description_about_us)
    TextView appDescripitions;

    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        context=this;
        ButterKnife.bind(this);
        initToolbar();

        initLastAppInfoData();


    }

    private void initLastAppInfoData() {
        if (!SharedPref.getKey(context,Constants.APP_INFO).equals("null")){
            appName.setText(SharedPref.getKey(context, Constants.APP_NAME));
            appVersion.setText(App.replaceBanglaFont(SharedPref.getKey(context, Constants.APP_VERSION)));
            appAuthor.setText(SharedPref.getKey(context, Constants.APP_AUTHOR));
            appName.setText(SharedPref.getKey(context, Constants.APP_NAME));
            appAuthorEmail.setText(SharedPref.getKey(context, Constants.APP_EMAIL));
            appAuthorWebsite.setText(SharedPref.getKey(context, Constants.APP_WEBSITE));
            getAppAuthorContact.setText(SharedPref.getKey(context, Constants.APP_CONTACT));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                appDescripitions.setText(Html.fromHtml(SharedPref.getKey(context, Constants.APP_DESCRIPTION), Html.FROM_HTML_MODE_COMPACT));
            } else {
                appDescripitions.setText(Html.fromHtml(SharedPref.getKey(context, Constants.APP_DESCRIPTION)));
            }
        }
    }


    private void initToolbar() {
        mToolbar=findViewById(R.id.toolbar_about_us);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.about_us_about));
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }


}

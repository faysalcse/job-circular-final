package com.jobcircular.android.Activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.jobcircular.android.R;
import com.jobcircular.android.Utils.Tools;
import com.jobcircular.android.database.Circular;
import com.jobcircular.android.database.CircularAdapter;
import com.jobcircular.android.database.CircularViewModel;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class BookMarkActivity extends AppCompatActivity {

    RecyclerView bookmarkCircular;
    CircularViewModel model;
    List<Circular> bookmarkCircularList;
    CircularAdapter bookmarkAdapter;

    Toolbar toolbar;
    LinearLayout warningLayout;
    LinearLayout contentLayout;


    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_mark);
        MobileAds.initialize(this);

        initToolbar(getString(R.string.title_bookmark));

        model= ViewModelProviders.of(this).get(CircularViewModel.class);
        bookmarkAdapter=new CircularAdapter(this);
        bookmarkCircular=findViewById(R.id.circularList);
        warningLayout=findViewById(R.id.warningLayout);
        contentLayout=findViewById(R.id.contentLayout);
        model.getAllNotes().observe(this, new Observer<List<Circular>>() {
            @Override
            public void onChanged(List<Circular> circulars) {

                if (circulars.size() !=0){
                    contentLayout.setVisibility(View.VISIBLE);
                    warningLayout.setVisibility(View.GONE);
                    bookmarkAdapter.setCirculars(circulars);
                    bookmarkCircular.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    bookmarkCircular.setAdapter(bookmarkAdapter);
                }else {
                    contentLayout.setVisibility(View.GONE);
                    warningLayout.setVisibility(View.VISIBLE);
                }


            }
        });


        setUpInterstitialAd();

    }

    private void setUpInterstitialAd() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.interstital_ads_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {


            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        displayInterstitial();
                    }
                }, 60000);
            }
        });

    }


    private void displayInterstitial() {

        if (mInterstitialAd != null) {

            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        }


    }

    private void initToolbar(String title) {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.colorTextAction));
        Tools.setSmartSystemBar(this);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}

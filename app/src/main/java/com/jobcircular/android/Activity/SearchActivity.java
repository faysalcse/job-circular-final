package com.jobcircular.android.Activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.jobcircular.android.Adapter.CircularAdapter;
import com.jobcircular.android.ApiService.APIClient;
import com.jobcircular.android.ApiService.APIInterface;
import com.jobcircular.android.Data.App;
import com.jobcircular.android.Models.circular.Circular;
import com.jobcircular.android.Models.circular.CircularList;
import com.jobcircular.android.R;
import com.jobcircular.android.Utils.Tools;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    Toolbar toolbar;
    ActionBar actionBar;

    APIInterface apiInterface;

    @BindView(R.id.circularList)
    RecyclerView circularList;


    @BindView(R.id.contentLayout)
    LinearLayout contentLayout;

    @BindView(R.id.failed_layout)
    LinearLayout failed_layout;

    @BindView(R.id.failed_message)
    TextView failed_message;

    @BindView(R.id.failed_retry)
    Button failed_retry;

    @BindView(R.id.progressBarLayout)
    LinearLayout progressBar;

    String id=null;

    Context context;
    String queryText=null;

    SearchView searchView;


    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        MobileAds.initialize(this);
        context=this;
        ButterKnife.bind(this);
        apiInterface= APIClient.getClient().create(APIInterface.class);



        initToolbar();
        inintErrorLayout(View.VISIBLE,getString(R.string.search_hint_main));
        failed_retry.setVisibility(View.GONE);
        setUpInterstitialAd();
    }


    private void setUpInterstitialAd() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.interstital_ads_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });

    }


    private void displayInterstitial() {

        if (mInterstitialAd != null) {

            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        }


    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.colorTextAction));
        Tools.setSmartSystemBar(this);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);




        searchView = searchView = (SearchView) searchItem.getActionView();
        if (searchView != null) {
            searchView.setIconified(false);
            searchView.setQueryHint(getString(R.string.search_hint));
            searchItem.expandActionView();

            TextView searchText = (TextView) searchView.findViewById(androidx.appcompat.R.id.search_src_text);
            Typeface myCustomFont = Typeface.createFromAsset(getAssets(),"fonts/bl_font.ttf");
            searchText.setTypeface(myCustomFont);
            searchView.setIconifiedByDefault(true);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {

                    if (query !=null && !TextUtils.isEmpty(query)){

                        if (App.isInternetOn(context)){
                            queryText=query;
                            initJobCircularByKeyward(query);
                        }else {
                            failed_layout.setVisibility(View.VISIBLE);
                            failed_message.setText(getString(R.string.error_internet));
                        }

                      //  searchItem.collapseActionView();

                    }else {
                        Toast.makeText(context, "দয়াকরে কিছু কিওয়ার্ড যুক্ত করুণ", Toast.LENGTH_SHORT).show();
                    }

                    searchView.clearFocus();

                    return false;
                }
                @Override
                public boolean onQueryTextChange(String s) {
                   inintErrorLayout(View.VISIBLE,getString(R.string.search_hint_main));
                   failed_retry.setVisibility(View.GONE);
                    return false;
                }
            });


            MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                   finish();
                    return true;
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void initJobCircularByKeyward(String searchText) {
        inintErrorLayout(View.GONE,"");
        progressBar.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.GONE);

        Call<CircularList> call=apiInterface.getCircularByKeyward(searchText);
        call.enqueue(new Callback<CircularList>() {
            @Override
            public void onResponse(Call<CircularList> call, Response<CircularList> response) {

                if (response.isSuccessful()){
                    List<Circular> circular_List=response.body().getCircularList();
                    if (circular_List.size() !=0){
                        initJobCircularView(circular_List);
                        progressBar.setVisibility(View.GONE);
                        inintErrorLayout(View.GONE,"");
                    }else {
                        inintErrorLayout(View.VISIBLE,getString(R.string.error_nodata));
                        progressBar.setVisibility(View.GONE);
                        failed_retry.setVisibility(View.GONE);
                    }
                }

                displayInterstitial();

            }

            @Override
            public void onFailure(Call<CircularList> call, Throwable t) {
                circularList.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                // Toasty.warning(ActivityJobsCategory.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                inintErrorLayout(View.VISIBLE,getString(R.string.error_server));
            }
        });

    }


    private void inintErrorLayout(int visivility,String  msg){
        switch (visivility){
            case View.GONE:
                failed_layout.setVisibility(View.GONE);
                contentLayout.setVisibility(View.VISIBLE);
                break;
            case View.VISIBLE:
                failed_layout.setVisibility(View.VISIBLE);
                contentLayout.setVisibility(View.GONE);
                failed_retry.setVisibility(View.VISIBLE);
                break;
        }
        if (msg !=null){
            failed_message.setText(msg);
            failed_retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (App.isInternetOn(SearchActivity.this)){
                        if (queryText !=null && TextUtils.isEmpty(queryText))
                                initJobCircularByKeyward(queryText);
                    }else {
                        failed_layout.setVisibility(View.VISIBLE);
                        failed_message.setText(getString(R.string.error_internet));
                    }
                }
            });
        }
    }

    private void initJobCircularView(List<Circular> data) {
        CircularAdapter adapter=new CircularAdapter(SearchActivity.this,data);
        LinearLayoutManager linearLayoutManager
                = new LinearLayoutManager(SearchActivity.this, LinearLayoutManager.VERTICAL, false);
      /*  DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(circularList.getContext(),
                linearLayoutManager.getOrientation());
        circularList.addItemDecoration(dividerItemDecoration);*/
        circularList.setLayoutManager(linearLayoutManager);
        circularList.setAdapter(adapter);
    }
}

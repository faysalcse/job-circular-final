package com.jobcircular.android.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;

import com.jobcircular.android.Data.App;

public class ProgressDialogEx extends ProgressDialog {
    Context context;
    public ProgressDialogEx(Context context) {
        super(context);
        this.context=context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = this.findViewById(android.R.id.message);
        if (view != null) {
            // Shouldn't be null. Just to be paranoid enough.

            Typeface ROBOTO_LIGHT_TYPE_FACE = Typeface.createFromAsset(context.getAssets(), "fonts/pts.ttf");
            App.setCustomTypeface(view, ROBOTO_LIGHT_TYPE_FACE);
        }
    }
}